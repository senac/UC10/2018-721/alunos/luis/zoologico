/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Zoologico;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sala304b
 */
public class ZoologicoTest {

    public static void main(String[] args) {

        Gato gato = new Gato();
        Cachorro cachorro = new Cachorro();
        Cobra cobra = new Cobra();
        Galinha galinha = new Galinha();
        Lagarto lagarto = new Lagarto();
        Pato pato = new Pato();
        Sapo sapo = new Sapo();
        Ornintorrinco ornitorrinco = new Ornintorrinco();

        List<Animal> colecao = new ArrayList<>();

        colecao.add(gato);
        colecao.add(cachorro);
        colecao.add(cobra);
        colecao.add(galinha);
        colecao.add(lagarto);
        colecao.add(pato);
        colecao.add(sapo);
        colecao.add(ornitorrinco);

        for (int i = 0; i < colecao.size(); i++) {
            colecao.get(i).andar();
            colecao.get(i).falar();

        }

    }

}
