package herança;

public class Automóvel extends Terrestre {

    private String cor;
    private int numerodeportas;
    private String placa;

    public String getCor() {
        return cor;
    }

    public void setCor(String Cor) {
        this.cor = Cor;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String Placa) {
        this.placa = Placa;
    }

    public int getnumerodeportas() {
        return numerodeportas;
    }

    public void setgetnumerodeportas(int numerodeportas) {
        this.numerodeportas = numerodeportas;
    }

}
